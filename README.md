## Project overview
Архетип для создания REST API клиента какого-либо веб-сервиса
при помощи библиотеки Retrofit 2
  
## Motivation

Очень часто приходится создавать проекты REST-клиентов, содержащие одни и те же зависимости
и одинаковый шаблонный код.
  
## Code Example

Для того, чтобы установить архетип введите в терминале команду

``` 
mvn install 
```

После этого архетип установится в локальный каталог.

Для того, чтобы теперь создать проект на основании кода архетипа,
введите в терминале следующую команду

**ключ -DarchetypeVersion должен ссылаться на текущую версию**

``` 
mvn archetype:generate \ 
-DarchetypeGroupId=com.pampushko.archetypes \ 
-DarchetypeArtifactId=retrofit-archetype \  
-DarchetypeVersion=0.2.7 
```
  
## Contributors

Alexander Pampushko   
   
## License
  
Apache License 2.0
