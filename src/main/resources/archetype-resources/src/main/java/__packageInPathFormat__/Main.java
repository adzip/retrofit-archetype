#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${groupId};

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.stream.Stream;

/**
 *
 */
public class Main
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static TestDogsApi testDogsApi;
	
	private Retrofit retrofit;
	
	public static void main(String[] args) throws IOException
	{
		Main app = new Main();
		app.start();
	}
	
	public void start()
	{
		try
		{
			retrofit = new Retrofit.Builder()
					.baseUrl("https://dog.ceo/api/breeds/list/all/")
					.addConverterFactory(GsonConverterFactory.create(new Gson()))
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.build();
			
			TestDogsApi testDogsApi = retrofit.create(TestDogsApi.class);
			Call<MainPojo> doge = testDogsApi.getDoge();
			Response<MainPojo> response = doge.execute();
			Message message = response.body().getMessage();
			
			doge.cancel();
			
			Stream.of(message.getTerrier()).forEach(System.out::println);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
