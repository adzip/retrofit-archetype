#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/**
 *
 */
public class MainPojo
{
	private Message message;
	
	private String status;
	
	public Message getMessage ()
	{
		return message;
	}
	
	public void setMessage (Message message)
	{
		this.message = message;
	}
	
	public String getStatus ()
	{
		return status;
	}
	
	public void setStatus (String status)
	{
		this.status = status;
	}
	
	@Override
	public String toString()
	{
		return "ClassPojo [message = "+message+", status = "+status+"]";
	}
}
