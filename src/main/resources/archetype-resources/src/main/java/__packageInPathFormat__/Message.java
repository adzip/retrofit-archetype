#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/**
 *
 */
public class Message
{
	private String[] germanshepherd;
	
	private String[] dane;
	
	private String[] collie;
	
	private String[] otterhound;
	
	private String[] mountain;
	
	private String[] maltese;
	
	private String[] eskimo;
	
	private String[] bullterrier;
	
	private String[] borzoi;
	
	private String[] ridgeback;
	
	private String[] mexicanhairless;
	
	private String[] deerhound;
	
	private String[] appenzeller;
	
	private String[] corgi;
	
	private String[] brabancon;
	
	private String[] komondor;
	
	private String[] husky;
	
	private String[] greyhound;
	
	private String[] beagle;
	
	private String[] kuvasz;
	
	private String[] pinscher;
	
	private String[] rottweiler;
	
	private String[] weimaraner;
	
	private String[] elkhound;
	
	private String[] schnauzer;
	
	private String[] pyrenees;
	
	private String[] dhole;
	
	private String[] pekinese;
	
	private String[] papillon;
	
	private String[] affenpinscher;
	
	private String[] labrador;
	
	private String[] saluki;
	
	private String[] bulldog;
	
	private String[] keeshond;
	
	private String[] hound;
	
	private String[] chow;
	
	private String[] pointer;
	
	private String[] bouvier;
	
	private String[] lhasa;
	
	private String[] mastiff;
	
	private String[] coonhound;
	
	private String[] doberman;
	
	private String[] sheepdog;
	
	private String[] retriever;
	
	private String[] springer;
	
	private String[] poodle;
	
	private String[] newfoundland;
	
	private String[] african;
	
	private String[] clumber;
	
	private String[] pembroke;
	
	private String[] pug;
	
	private String[] airedale;
	
	private String[] akita;
	
	private String[] wolfhound;
	
	private String[] groenendael;
	
	private String[] schipperke;
	
	private String[] redbone;
	
	private String[] dingo;
	
	private String[] shihtzu;
	
	private String[] setter;
	
	private String[] vizsla;
	
	private String[] malamute;
	
	private String[] whippet;
	
	private String[] kelpie;
	
	private String[] spaniel;
	
	private String[] dachshund;
	
	private String[] boxer;
	
	private String[] entlebucher;
	
	private String[] terrier;
	
	private String[] samoyed;
	
	private String[] malinois;
	
	private String[] stbernard;
	
	private String[] shiba;
	
	private String[] bluetick;
	
	private String[] leonberg;
	
	private String[] basenji;
	
	private String[] cairn;
	
	private String[] chihuahua;
	
	private String[] pomeranian;
	
	private String[] briard;
	
	public String[] getGermanshepherd ()
	{
		return germanshepherd;
	}
	
	public void setGermanshepherd (String[] germanshepherd)
	{
		this.germanshepherd = germanshepherd;
	}
	
	public String[] getDane ()
	{
		return dane;
	}
	
	public void setDane (String[] dane)
	{
		this.dane = dane;
	}
	
	public String[] getCollie ()
	{
		return collie;
	}
	
	public void setCollie (String[] collie)
	{
		this.collie = collie;
	}
	
	public String[] getOtterhound ()
	{
		return otterhound;
	}
	
	public void setOtterhound (String[] otterhound)
	{
		this.otterhound = otterhound;
	}
	
	public String[] getMountain ()
	{
		return mountain;
	}
	
	public void setMountain (String[] mountain)
	{
		this.mountain = mountain;
	}
	
	public String[] getMaltese ()
	{
		return maltese;
	}
	
	public void setMaltese (String[] maltese)
	{
		this.maltese = maltese;
	}
	
	public String[] getEskimo ()
	{
		return eskimo;
	}
	
	public void setEskimo (String[] eskimo)
	{
		this.eskimo = eskimo;
	}
	
	public String[] getBullterrier ()
	{
		return bullterrier;
	}
	
	public void setBullterrier (String[] bullterrier)
	{
		this.bullterrier = bullterrier;
	}
	
	public String[] getBorzoi ()
	{
		return borzoi;
	}
	
	public void setBorzoi (String[] borzoi)
	{
		this.borzoi = borzoi;
	}
	
	public String[] getRidgeback ()
	{
		return ridgeback;
	}
	
	public void setRidgeback (String[] ridgeback)
	{
		this.ridgeback = ridgeback;
	}
	
	public String[] getMexicanhairless ()
	{
		return mexicanhairless;
	}
	
	public void setMexicanhairless (String[] mexicanhairless)
	{
		this.mexicanhairless = mexicanhairless;
	}
	
	public String[] getDeerhound ()
	{
		return deerhound;
	}
	
	public void setDeerhound (String[] deerhound)
	{
		this.deerhound = deerhound;
	}
	
	public String[] getAppenzeller ()
	{
		return appenzeller;
	}
	
	public void setAppenzeller (String[] appenzeller)
	{
		this.appenzeller = appenzeller;
	}
	
	public String[] getCorgi ()
	{
		return corgi;
	}
	
	public void setCorgi (String[] corgi)
	{
		this.corgi = corgi;
	}
	
	public String[] getBrabancon ()
	{
		return brabancon;
	}
	
	public void setBrabancon (String[] brabancon)
	{
		this.brabancon = brabancon;
	}
	
	public String[] getKomondor ()
	{
		return komondor;
	}
	
	public void setKomondor (String[] komondor)
	{
		this.komondor = komondor;
	}
	
	public String[] getHusky ()
	{
		return husky;
	}
	
	public void setHusky (String[] husky)
	{
		this.husky = husky;
	}
	
	public String[] getGreyhound ()
	{
		return greyhound;
	}
	
	public void setGreyhound (String[] greyhound)
	{
		this.greyhound = greyhound;
	}
	
	public String[] getBeagle ()
	{
		return beagle;
	}
	
	public void setBeagle (String[] beagle)
	{
		this.beagle = beagle;
	}
	
	public String[] getKuvasz ()
	{
		return kuvasz;
	}
	
	public void setKuvasz (String[] kuvasz)
	{
		this.kuvasz = kuvasz;
	}
	
	public String[] getPinscher ()
	{
		return pinscher;
	}
	
	public void setPinscher (String[] pinscher)
	{
		this.pinscher = pinscher;
	}
	
	public String[] getRottweiler ()
	{
		return rottweiler;
	}
	
	public void setRottweiler (String[] rottweiler)
	{
		this.rottweiler = rottweiler;
	}
	
	public String[] getWeimaraner ()
	{
		return weimaraner;
	}
	
	public void setWeimaraner (String[] weimaraner)
	{
		this.weimaraner = weimaraner;
	}
	
	public String[] getElkhound ()
	{
		return elkhound;
	}
	
	public void setElkhound (String[] elkhound)
	{
		this.elkhound = elkhound;
	}
	
	public String[] getSchnauzer ()
	{
		return schnauzer;
	}
	
	public void setSchnauzer (String[] schnauzer)
	{
		this.schnauzer = schnauzer;
	}
	
	public String[] getPyrenees ()
	{
		return pyrenees;
	}
	
	public void setPyrenees (String[] pyrenees)
	{
		this.pyrenees = pyrenees;
	}
	
	public String[] getDhole ()
	{
		return dhole;
	}
	
	public void setDhole (String[] dhole)
	{
		this.dhole = dhole;
	}
	
	public String[] getPekinese ()
	{
		return pekinese;
	}
	
	public void setPekinese (String[] pekinese)
	{
		this.pekinese = pekinese;
	}
	
	public String[] getPapillon ()
	{
		return papillon;
	}
	
	public void setPapillon (String[] papillon)
	{
		this.papillon = papillon;
	}
	
	public String[] getAffenpinscher ()
	{
		return affenpinscher;
	}
	
	public void setAffenpinscher (String[] affenpinscher)
	{
		this.affenpinscher = affenpinscher;
	}
	
	public String[] getLabrador ()
	{
		return labrador;
	}
	
	public void setLabrador (String[] labrador)
	{
		this.labrador = labrador;
	}
	
	public String[] getSaluki ()
	{
		return saluki;
	}
	
	public void setSaluki (String[] saluki)
	{
		this.saluki = saluki;
	}
	
	public String[] getBulldog ()
	{
		return bulldog;
	}
	
	public void setBulldog (String[] bulldog)
	{
		this.bulldog = bulldog;
	}
	
	public String[] getKeeshond ()
	{
		return keeshond;
	}
	
	public void setKeeshond (String[] keeshond)
	{
		this.keeshond = keeshond;
	}
	
	public String[] getHound ()
	{
		return hound;
	}
	
	public void setHound (String[] hound)
	{
		this.hound = hound;
	}
	
	public String[] getChow ()
	{
		return chow;
	}
	
	public void setChow (String[] chow)
	{
		this.chow = chow;
	}
	
	public String[] getPointer ()
	{
		return pointer;
	}
	
	public void setPointer (String[] pointer)
	{
		this.pointer = pointer;
	}
	
	public String[] getBouvier ()
	{
		return bouvier;
	}
	
	public void setBouvier (String[] bouvier)
	{
		this.bouvier = bouvier;
	}
	
	public String[] getLhasa ()
	{
		return lhasa;
	}
	
	public void setLhasa (String[] lhasa)
	{
		this.lhasa = lhasa;
	}
	
	public String[] getMastiff ()
	{
		return mastiff;
	}
	
	public void setMastiff (String[] mastiff)
	{
		this.mastiff = mastiff;
	}
	
	public String[] getCoonhound ()
	{
		return coonhound;
	}
	
	public void setCoonhound (String[] coonhound)
	{
		this.coonhound = coonhound;
	}
	
	public String[] getDoberman ()
	{
		return doberman;
	}
	
	public void setDoberman (String[] doberman)
	{
		this.doberman = doberman;
	}
	
	public String[] getSheepdog ()
	{
		return sheepdog;
	}
	
	public void setSheepdog (String[] sheepdog)
	{
		this.sheepdog = sheepdog;
	}
	
	public String[] getRetriever ()
	{
		return retriever;
	}
	
	public void setRetriever (String[] retriever)
	{
		this.retriever = retriever;
	}
	
	public String[] getSpringer ()
	{
		return springer;
	}
	
	public void setSpringer (String[] springer)
	{
		this.springer = springer;
	}
	
	public String[] getPoodle ()
	{
		return poodle;
	}
	
	public void setPoodle (String[] poodle)
	{
		this.poodle = poodle;
	}
	
	public String[] getNewfoundland ()
	{
		return newfoundland;
	}
	
	public void setNewfoundland (String[] newfoundland)
	{
		this.newfoundland = newfoundland;
	}
	
	public String[] getAfrican ()
	{
		return african;
	}
	
	public void setAfrican (String[] african)
	{
		this.african = african;
	}
	
	public String[] getClumber ()
	{
		return clumber;
	}
	
	public void setClumber (String[] clumber)
	{
		this.clumber = clumber;
	}
	
	public String[] getPembroke ()
	{
		return pembroke;
	}
	
	public void setPembroke (String[] pembroke)
	{
		this.pembroke = pembroke;
	}
	
	public String[] getPug ()
	{
		return pug;
	}
	
	public void setPug (String[] pug)
	{
		this.pug = pug;
	}
	
	public String[] getAiredale ()
	{
		return airedale;
	}
	
	public void setAiredale (String[] airedale)
	{
		this.airedale = airedale;
	}
	
	public String[] getAkita ()
	{
		return akita;
	}
	
	public void setAkita (String[] akita)
	{
		this.akita = akita;
	}
	
	public String[] getWolfhound ()
	{
		return wolfhound;
	}
	
	public void setWolfhound (String[] wolfhound)
	{
		this.wolfhound = wolfhound;
	}
	
	public String[] getGroenendael ()
	{
		return groenendael;
	}
	
	public void setGroenendael (String[] groenendael)
	{
		this.groenendael = groenendael;
	}
	
	public String[] getSchipperke ()
	{
		return schipperke;
	}
	
	public void setSchipperke (String[] schipperke)
	{
		this.schipperke = schipperke;
	}
	
	public String[] getRedbone ()
	{
		return redbone;
	}
	
	public void setRedbone (String[] redbone)
	{
		this.redbone = redbone;
	}
	
	public String[] getDingo ()
	{
		return dingo;
	}
	
	public void setDingo (String[] dingo)
	{
		this.dingo = dingo;
	}
	
	public String[] getShihtzu ()
	{
		return shihtzu;
	}
	
	public void setShihtzu (String[] shihtzu)
	{
		this.shihtzu = shihtzu;
	}
	
	public String[] getSetter ()
	{
		return setter;
	}
	
	public void setSetter (String[] setter)
	{
		this.setter = setter;
	}
	
	public String[] getVizsla ()
	{
		return vizsla;
	}
	
	public void setVizsla (String[] vizsla)
	{
		this.vizsla = vizsla;
	}
	
	public String[] getMalamute ()
	{
		return malamute;
	}
	
	public void setMalamute (String[] malamute)
	{
		this.malamute = malamute;
	}
	
	public String[] getWhippet ()
	{
		return whippet;
	}
	
	public void setWhippet (String[] whippet)
	{
		this.whippet = whippet;
	}
	
	public String[] getKelpie ()
	{
		return kelpie;
	}
	
	public void setKelpie (String[] kelpie)
	{
		this.kelpie = kelpie;
	}
	
	public String[] getSpaniel ()
	{
		return spaniel;
	}
	
	public void setSpaniel (String[] spaniel)
	{
		this.spaniel = spaniel;
	}
	
	public String[] getDachshund ()
	{
		return dachshund;
	}
	
	public void setDachshund (String[] dachshund)
	{
		this.dachshund = dachshund;
	}
	
	public String[] getBoxer ()
	{
		return boxer;
	}
	
	public void setBoxer (String[] boxer)
	{
		this.boxer = boxer;
	}
	
	public String[] getEntlebucher ()
	{
		return entlebucher;
	}
	
	public void setEntlebucher (String[] entlebucher)
	{
		this.entlebucher = entlebucher;
	}
	
	public String[] getTerrier ()
	{
		return terrier;
	}
	
	public void setTerrier (String[] terrier)
	{
		this.terrier = terrier;
	}
	
	public String[] getSamoyed ()
	{
		return samoyed;
	}
	
	public void setSamoyed (String[] samoyed)
	{
		this.samoyed = samoyed;
	}
	
	public String[] getMalinois ()
	{
		return malinois;
	}
	
	public void setMalinois (String[] malinois)
	{
		this.malinois = malinois;
	}
	
	public String[] getStbernard ()
	{
		return stbernard;
	}
	
	public void setStbernard (String[] stbernard)
	{
		this.stbernard = stbernard;
	}
	
	public String[] getShiba ()
	{
		return shiba;
	}
	
	public void setShiba (String[] shiba)
	{
		this.shiba = shiba;
	}
	
	public String[] getBluetick ()
	{
		return bluetick;
	}
	
	public void setBluetick (String[] bluetick)
	{
		this.bluetick = bluetick;
	}
	
	public String[] getLeonberg ()
	{
		return leonberg;
	}
	
	public void setLeonberg (String[] leonberg)
	{
		this.leonberg = leonberg;
	}
	
	public String[] getBasenji ()
	{
		return basenji;
	}
	
	public void setBasenji (String[] basenji)
	{
		this.basenji = basenji;
	}
	
	public String[] getCairn ()
	{
		return cairn;
	}
	
	public void setCairn (String[] cairn)
	{
		this.cairn = cairn;
	}
	
	public String[] getChihuahua ()
	{
		return chihuahua;
	}
	
	public void setChihuahua (String[] chihuahua)
	{
		this.chihuahua = chihuahua;
	}
	
	public String[] getPomeranian ()
	{
		return pomeranian;
	}
	
	public void setPomeranian (String[] pomeranian)
	{
		this.pomeranian = pomeranian;
	}
	
	public String[] getBriard ()
	{
		return briard;
	}
	
	public void setBriard (String[] briard)
	{
		this.briard = briard;
	}
	
	@Override
	public String toString()
	{
		return "ClassPojo [germanshepherd = "+germanshepherd+", dane = "+dane+", collie = "+collie+", otterhound = "+otterhound+", mountain = "+mountain+", maltese = "+maltese+", eskimo = "+eskimo+", bullterrier = "+bullterrier+", borzoi = "+borzoi+", ridgeback = "+ridgeback+", mexicanhairless = "+mexicanhairless+", deerhound = "+deerhound+", appenzeller = "+appenzeller+", corgi = "+corgi+", brabancon = "+brabancon+", komondor = "+komondor+", husky = "+husky+", greyhound = "+greyhound+", beagle = "+beagle+", kuvasz = "+kuvasz+", pinscher = "+pinscher+", rottweiler = "+rottweiler+", weimaraner = "+weimaraner+", elkhound = "+elkhound+", schnauzer = "+schnauzer+", pyrenees = "+pyrenees+", dhole = "+dhole+", pekinese = "+pekinese+", papillon = "+papillon+", affenpinscher = "+affenpinscher+", labrador = "+labrador+", saluki = "+saluki+", bulldog = "+bulldog+", keeshond = "+keeshond+", hound = "+hound+", chow = "+chow+", pointer = "+pointer+", bouvier = "+bouvier+", lhasa = "+lhasa+", mastiff = "+mastiff+", coonhound = "+coonhound+", doberman = "+doberman+", sheepdog = "+sheepdog+", retriever = "+retriever+", springer = "+springer+", poodle = "+poodle+", newfoundland = "+newfoundland+", african = "+african+", clumber = "+clumber+", pembroke = "+pembroke+", pug = "+pug+", airedale = "+airedale+", akita = "+akita+", wolfhound = "+wolfhound+", groenendael = "+groenendael+", schipperke = "+schipperke+", redbone = "+redbone+", dingo = "+dingo+", shihtzu = "+shihtzu+", setter = "+setter+", vizsla = "+vizsla+", malamute = "+malamute+", whippet = "+whippet+", kelpie = "+kelpie+", spaniel = "+spaniel+", dachshund = "+dachshund+", boxer = "+boxer+", entlebucher = "+entlebucher+", terrier = "+terrier+", samoyed = "+samoyed+", malinois = "+malinois+", stbernard = "+stbernard+", shiba = "+shiba+", bluetick = "+bluetick+", leonberg = "+leonberg+", basenji = "+basenji+", cairn = "+cairn+", chihuahua = "+chihuahua+", pomeranian = "+pomeranian+", briard = "+briard+"]";
	}
}
