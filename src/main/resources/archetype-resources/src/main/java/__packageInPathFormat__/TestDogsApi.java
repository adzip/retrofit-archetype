#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${groupId};

import retrofit2.Call;
import retrofit2.http.GET;

/**
 *
 */
public interface TestDogsApi
{
	@GET("/api/breeds/list/all")
	Call<MainPojo> getDoge();
}
